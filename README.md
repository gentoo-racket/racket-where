# Racket Package Finder

<p align="center">
    <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/gentoo-racket/racket-where">
        <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/gentoo-racket/racket-where/">
    </a>
    <a href="https://gitlab.com/gentoo-racket/racket-where/pipelines">
        <img src="https://gitlab.com/gentoo-racket/racket-where/badges/master/pipeline.svg">
    </a>
</p>


## About

Small tool to find Racket packages.
Also used to detect whether a package is installed.


## License

This file is part of racket-where - find location of Racket packages.

Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
